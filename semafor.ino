volatile char *led = (char *) 0xffffff10;
/* resetIn - ulaz btn_down (ekvivalentno signalu vanjski_reset) */
volatile char *resetIn = (char *) 0xffffff00;

volatile char *tl = (char *) 0xffffff13;
volatile char *tr = (char *) 0xffffff12;


/* zvucni signal */
#define MIDI_A0  57
/* pauza - tisina */
#define MIDI_P   0

/* pocetno stanje automata (default: vanjski reset = 0) */
int s = 2;

/* a - vrijednost vektora led[7] downto led[4] (prikaz semafora - automobili) */
/* b - vrijednost vektora led[3] downto led[0] (prikaz semafora - pjesaci) */
static void playLed(int a, int p, int t, int sndTimes) {
  int i;

  for (i = 0; i < t; ++i) {
    *led = (a << 4) | (p & 0x0f);
    playSound(sndTimes);
  }
}


static void playSound(int snd) {
  const int sndDur = 200;
  const int pauseDur = 50;
  int i;
  if (snd == 4) {
    for (i = 0; i < snd; ++i) {
      *tl = MIDI_A0;
      *tr = MIDI_A0;
      delay(sndDur);

      *tl = MIDI_P;
      *tr = MIDI_P;
      delay(pauseDur);
    }
  } else if (snd == 1) {
    *tl = MIDI_A0;
    *tr = MIDI_A0;
    delay(sndDur);
    *tl = MIDI_P;
    *tr = MIDI_P;
    delay(1000 - sndDur);
  } else {
    *tl = MIDI_P;
    *tr = MIDI_P;
    delay(1000);
  }
}


void setup() {
}

void loop() {
  switch (s) {
    case 0:
      playLed(4, 0, 1, 0);
      break;
    case 1:
      playLed(0, 0, 1, 0);
      break;
    case 2:
      playLed(4, 8, 4, 1);
      break;
    case 3:
      playLed(8, 8, 3, 1);
      break;
    case 4:
      playLed(8, 4, 5, 4);
      break;
    case 5:
      playLed(8, 8, 4, 1);
      break;
    case 6:
      playLed(12, 8, 3, 1);
      break;
    case 7:
      playLed(2, 8, 10, 1);
      break;
    } 

  /* resetIn - input vrijednost 4 za btn_down (odgovara signalu vanjski_reset) */
  if (*resetIn == 4 && s != 0) {
    s = 0;
  } else if (*resetIn == 4 && s == 0) {
    s = 1; 
  } else if (*resetIn == 0 && (s == 0 || s == 1 || s == 7)) {
    s = 2;
  } else {
    s++;
  }

}
