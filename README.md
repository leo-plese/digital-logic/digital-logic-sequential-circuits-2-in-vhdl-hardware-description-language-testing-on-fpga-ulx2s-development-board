# Digital Logic Sequential Circuits 2 in VHDL Hardware Description Language Testing on FPGA ULX2S Development Board

Digital Logic Circuits developed in VHDL Very High-Speed Integrated Circuit Hardware Description Language.

The program "semafor.ino" implements automatic semaphore control using FPGA as a microprocessor system. It is developed in C in Arduino IDE (https://www.arduino.cc/).

My lab assignment in Digital Logic, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2017
